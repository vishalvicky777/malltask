import { LightningElement, track, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getMallDetails from '@salesforce/apex/MallDirectory.getMallDetails';

export default class MallViewDirectory extends NavigationMixin( LightningElement ) {

    //comment: *Columns for the mall view component*
    gridColumns = [
        { label: 'Mall', fieldName : 'Name', type: 'text',  sortable : true},
        { label: 'Country', fieldName : 'Country__c', type: 'text',  sortable : true},
        { label: 'Location', fieldName : 'Location__c', type: 'text',  sortable : true},
        {type: 'button',
        typeAttributes: {
            label: 'View'
        }
        }];

    @api recordId;   
    gridData = []
    
    //comment: *handle method for expand button*
    @wire(getMallDetails,{recordId : '$recordId'})
    mallViewData({ error, data }){
        console.log( 'Inside wire' );
        if(data){
                this.gridData = data.map(item=>{
                    const {Store_Categories__r, Brands__r, ...mall} = item
                        const allBrands = Store_Categories__r.map(cat=>{
                        return {...cat, "_children":Brands__r}
                        })
                    return {...mall, "_children":allBrands}
                    })
                } 
        else if ( error ) {
            console.error(error);
        }

    }

    //comment: *handle method for expand button*
    clickToExpandAll( e ) {
        const grid =  this.template.querySelector( 'lightning-tree-grid' );
        grid.expandAll();
    }

     //comment: *handle method for Collapse button*
    clickToCollapseAll( e ) {
        const grid =  this.template.querySelector( 'lightning-tree-grid' );
        grid.collapseAll();
     
    }

    //comment: *handle method for Collapse button*
    handleRowAction( event ) {
        const row = event.detail.row;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: row.Id,
                actionName: 'view'   
            }
        });

    }

}