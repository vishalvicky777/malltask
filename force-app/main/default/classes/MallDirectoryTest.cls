/*Test Coverage for MallDirectory*/

@isTest
private with sharing class MallDirectoryTest {
    static testMethod void directory(){
        Mall__c mdata = new Mall__c(Name = 'Dummy Mall', Country__c = 'Dummy Country', Location__c = 'Dummy Area');
        insert mdata;
        Store_Category__c sCat = new Store_Category__c(Name = 'Dummy Mall',Country__c = 'Dummy Country', Location__c = 'Dummy Area', Mall__c = mdata.id);
        insert sCat;
        Brands__c brand = new Brands__c(Name = 'Dummy Brand', Mall__c = mdata.id, Store_Category__c = sCat.id);
        insert brand;
        List<Mall__c> mallList = MallDirectory.getMallDetails(mdata.id);
    }
}