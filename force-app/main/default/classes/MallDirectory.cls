public class MallDirectory {
    @AuraEnabled(cacheable=true)
    public static List < Mall__c > getMallDetails(Id recordId) {

        return  [SELECT id, Name, Country__c, Location__c, 
                (Select id, Name, Country__c, Location__c from Store_Categories__r), 
                (Select id, Name from Brands__r) 
                FROM Mall__c
                WHERE Id=:recordId];      
    }
   
}